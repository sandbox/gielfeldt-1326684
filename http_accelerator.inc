<?php

$conf['page_cache_fastpath'] = TRUE;

function page_cache_fastpath() {
  if (!variable_get('http_accelerator_enabled', FALSE)) {
    return FALSE;
  }

  include_once dirname(__FILE__) . '/HTTPAccelerator.class.php';
  if (HTTPAccelerator::initialize() && HTTPAccelerator::execute()) {
    return TRUE;
  }
  return FALSE;
}

