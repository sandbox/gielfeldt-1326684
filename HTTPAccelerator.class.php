<?php

$GLOBALS['HTTP_ACCELERATOR_LOADED'] = TRUE;
include_once(dirname(__FILE__) . '/http_accelerator.defines.inc');

register_shutdown_function('cwdfix');
function cwdfix() {
  chdir($_SERVER['DOCUMENT_ROOT']);
}

class HTTPAccelerator {
  static public $base_root;
  static public $request_uri;
  static public $initialized = FALSE;
  /* static public $esi_pattern = '[(<esi:include.+?src="(.*?)".*?>(</esi.*?>)?)]';*/
  static public $esi_pattern = '/<esi:include.+src="([^"]*)".*\/>/Us';
  static public $force = FALSE;

  // Settings
  static public $cache_dir = '';
  static public $cache = FALSE;
  static public $esi = FALSE;
  static public $timeout = 0;
  static public $lock_timeout = 0;
  static public $graceful_timeout = 0;
  static public $queue = FALSE;

  public $url;
  public $path;
  public $content_group = 'default';
  public $esi_urls = NULL;
  public $esi_content = array();
  public $expires = NULL;
  public $is_cacheable = NULL;
  public $is_loaded = FALSE;
 
  public function __construct($url) {
    $this->url = $url;
    $this->path = self::$cache_dir . '/' . $this->cachepath();
    $this->file = $this->path . '/index.php';
  }

  public function lock($hard = FALSE, $retry = 3) {
    if ($retry <= 0) {
      return FALSE;
    }

    if ($hard) {
      if ($fp = @fopen($this->file . '.lock.hard', 'x')) {
        $this->log('lock hard');
        return TRUE;
      }
      else {
        return FALSE;
      }
    }

    if ($fp = @fopen($this->file . '.lock', 'x')) {
      fclose($fp);
      $this->log('lock');
      return TRUE;
    }

    // If file is locked, then unlock if lock is older than 2 minutes.
    $stat = stat($this->file . '.lock');
    if ($stat['mtime'] + self::$lock_timeout < time()) {
      $this->unlock();
      return $this->lock($hard, $retry - 1);
    }

    $this->log('no lock');
    return FALSE;
  }

  public function unlock() {
    $this->log('unlock');
    @unlink($this->file . '.lock');
    @unlink($this->file . '.lock.hard');
  }

  public function cachePath() {
    $sep = '{}';
    $url = parse_url($this->url);
    $url['query'] = empty($url['query']) ? 'noqs' : 'qs.' . $url['query'];
    $path = $this->content_group . '/' . $url['host'] . '/' . $url['query'] . '/' . $sep . str_replace('/', $sep, trim($url['path'], '/'));
    return $path;
  }

  public function isCacheable() {
    if (isset($this->is_cacheable)) {
      return $this->is_cacheable;
    }

    $this->is_cacheable = function_exists('custom_http_accelerator_is_cacheable') ? custom_http_accelerator_is_cacheable($this) : self::isCacheableDefault($this);
    if ($this->is_cacheable) {
      @mkdir($this->path, 0777, TRUE);
    }
    return $this->is_cacheable;
  }

  static public function isCacheableDefault($cache) {
    // Don't cache cron
    if (basename($_SERVER['PHP_SELF']) == 'cron.php') {
      return FALSE;
    }

    // Only cache GET requests
    if ($_SERVER['REQUEST_METHOD'] != 'GET' && $_SERVER['REQUEST_METHOD'] != 'HEAD') {
      return FALSE;
    }

    // Don't cache users with sessions
    foreach ($_COOKIE as $key => $value) {
      if (strpos($key, 'SESS') !== FALSE) {
        return FALSE;
      }
    }

    return TRUE;
  }

  public function exists() {
    return file_exists($this->file);
  }

  public function load() {
    if ($this->is_loaded) {
      return TRUE;
    }
    if (file_exists($this->file)) {
      $this->is_loaded = TRUE;
      $cache = $this;
      include($this->file);
      return TRUE;
    }
    return FALSE;
  }

  public function deliverHeaders() {
    call_user_func($this->deliverHeaders);
  }

  public function getContent() {
    $content = file_get_contents($this->content_file);
    if (self::$esi) {
      $content = $this->applyESICache($content);
    }
    return $content;
  }

  public function store($content) {
    $this->log('store');
    // Set cache headers
    header("Last-Modified: " . gmdate('D, d M Y H:i:s', $this->time) . ' GMT');

    $time = $this->time;

    // Determine if any esi tags are present
    $this->determineESI($content);

    $stub = '<' . '?php ';
    $stub .= '$deliver = FALSE;' . "\n";
    $stub .= 'if (empty($GLOBALS["HTTP_ACCELERATOR_LOADED"])) {' . "\n";
      $stub .= 'include("' . dirname(__FILE__) . '/HTTPAccelerator.class.php");' . "\n";
      $stub .= 'HTTPAccelerator::$base_root = ' . var_export(self::$base_root, TRUE) . ';' . "\n";
      $stub .= 'HTTPAccelerator::$request_uri = ' . var_export(self::$request_uri, TRUE) . ';' . "\n";
      $stub .= 'HTTPAccelerator::$cache_dir = ' . var_export(self::$cache_dir, TRUE) . ';' . "\n";
      $stub .= 'HTTPAccelerator::$esi = ' . var_export(self::$esi, TRUE) . ';' . "\n";
      $stub .= 'HTTPAccelerator::$cache = ' . var_export(self::$cache, TRUE) . ';' . "\n";
      $stub .= 'HTTPAccelerator::$timeout = ' . var_export(self::$timeout, TRUE) . ';' . "\n";
      $stub .= 'HTTPAccelerator::$lock_timeout = ' . var_export(self::$lock_timeout, TRUE) . ';' . "\n";
      $stub .= 'HTTPAccelerator::$graceful_timeout = ' . var_export(self::$graceful_timeout, TRUE) . ';' . "\n";
      $stub .= '$cache = new HTTPAccelerator(HTTPAccelerator::$base_root . HTTPAccelerator::$request_uri);' . "\n";
      $stub .= '$deliver = TRUE;' . "\n";
    $stub .= '}' . "\n";

    $stub .= '$cache->expires = ' . var_export($this->expires, TRUE) . ';' . "\n";
    $stub .= '$cache->modified = ' . var_export($this->time, TRUE) . ';' . "\n";
    $stub .= '$cache->content_file = dirname(__FILE__) . "/index.php.content";' . "\n";

    if (empty($this->esi_urls)) {
      header("Expires: " . gmdate('D, d M Y H:i:s', $this->expires) . ' GMT');
    }

    $stub .= 'if ($deliver) {' . "\n";
    $stub .= 'if ($_SERVER["REQUEST_TIME"] > $cache->expires) {' . "\n";
    $stub .= '$cache->initialized = FALSE;' . "\n";
    $stub .= '$_SERVER["SCRIPT_FILENAME"] = ' . var_export($_SERVER['SCRIPT_FILENAME'], TRUE) . ';' . "\n";
    $stub .= '$_SERVER["SCRIPT_NAME"] = ' . var_export($_SERVER['SCRIPT_NAME'], TRUE) . ';' . "\n";
    $stub .= '$_SERVER["PHP_SELF"] = ' . var_export($_SERVER['PHP_SELF'], TRUE) . ';' . "\n";
    $stub .= '$_GET["q"] = ' . var_export($_GET['q'], TRUE) . ';' . "\n";
    $stub .= '$_REQUEST["q"] = ' . var_export($_REQUEST['q'], TRUE) . ';' . "\n";
    $stub .= 'chdir(' . var_export($_SERVER['DOCUMENT_ROOT'], TRUE) . ');' . "\n";
    $stub .= 'include(' . var_export($_SERVER['DOCUMENT_ROOT'] . $_SERVER['PHP_SELF'], TRUE) . ');' . "\n";
    $stub .= 'exit;' . "\n";
    $stub .= '}' . "\n";

    if (empty($this->esi_urls)) {
      $stub .= '$cache->exitIfModified();' . "\n";
    }
    $stub .= '}' . "\n";

    $headers = self::filterResponseHeaders();
    $stub .= '$cache->deliverHeaders = function() {' . "\n";
    $used = array();
    foreach ($headers as $header) {
      list($key, $value) = explode(': ', $header, 2);
      $stub .= 'header(' . var_export($header, TRUE) . ', ' . (isset($used[$key]) ? 'FALSE' : 'TRUE') . ');' . "\n";
      $used[$key] = TRUE;
    }
    $stub .= '};' ."\n";

    $stub .= 'if ($deliver) {' . "\n";

    $cache_content = $content;

    // If esi tags are present, then create a php file
    if (!empty($this->esi_urls)) {
      // Create php file
      if (self::$esi) {
        $stub .= 'ob_start(array($cache, "applyESIContent"));' . "\n";
      }
    }
    $stub .= '$cache->deliverHeaders();' . "\n";
    $stub .= 'print $cache->getContent();' . "\n";
    $stub .= '}' ."\n";

    // @todo Replace php open/close tags here with equivalent print statements.

    // Create html file
    $tempfile = tempnam(dirname($this->file), 'http-accelerator');
    file_put_contents($tempfile, $stub);

    $tempfilecontent = tempnam(dirname($this->file), 'http-accelerator');
    file_put_contents($tempfilecontent, $cache_content);

    // Update cache file
    @unlink($this->file);
    @unlink($this->file . '.content');
    @rename($tempfilecontent, $this->file . '.content');
    @rename($tempfile, $this->file);

    $this->unlock();

    return $content;
  }

  public function isExpired() {
    $this->load();
    
    if ($_SERVER["REQUEST_TIME"] > $this->expires) {
      return TRUE;
    }
    return FALSE;
  }

  public function determineESI($content) {
    if (!isset($this->esi_urls)) {
      $this->esi_urls = array();
      preg_match_all(self::$esi_pattern, $content, $matches);
      // error_log(serialize($matches));
      foreach ($matches[1] as $i => $url) {
        $parsed = parse_url($url);
        if (empty($parsed['host'])) {
          $url = self::$base_root . $url;
        }
        $this->esi_urls[$url] = $matches[0][$i];
      }
    }
  }

  function log($msg) {
    $url = parse_url($this->url);
    $path = $url['path'];
    error_log('[' . getmypid() . "] [$path]: $msg");
  }

  public function applyESICache($content) {
    $this->determineESI($content);
    foreach ($this->esi_urls as $url => $tag) {
      $cache = new HTTPAccelerator($url);
      if ($cache->exists() && !$cache->isExpired()) {
        $content = str_replace($tag, $cache->getContent(), $content);
      }
    }
    return $content;
  }

  public function applyESIContent($content) {
    // terror_log("Applying ESI");
    $this->determineESI($content);
    if (empty($this->esi_urls)) {
      return $content;
    }
    if (function_exists('curl_multi_init')) {
      return $this->applyESIContentCurl($content);
    }
    else {
      return $this->applyESIContentDefault($content);
    }
  }

  public function applyESIContentDefault($content) {
    $headers = self::filterRequestHeaders();
    foreach (array_keys($headers) as $idx) {
      list($key, $value) = explode(': ', $headers[$idx], 2);
      $headers[$key] = $value;
      unset($headers[$idx]);
    }
    foreach ($this->esi_urls as $url => $tag) {
      $response = $this->http_request($url, $headers);
      $this->esi_content[$url] = $response->data;
      $content = str_replace($tag, $this->esi_content[$url], $content);
    }
    return $content;
  }

  public function applyESIContentCurl($content) {
    $mh = curl_multi_init();
    foreach ($this->esi_urls as $url => $tag) {
      // @todo Load from cache here if possible?
      //       Be aware that ob_start won't work, and cache is php code ... how to fix?

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_HEADER, FALSE);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
      curl_setopt($ch, CURLOPT_HTTPHEADER, self::filterRequestHeaders());
      curl_multi_add_handle($mh, $ch);
      $chs[$url] = $ch;
    }
 
    // error_log('starting multi curl');
    do {
      while (($execrun = curl_multi_exec($mh, $running)) == CURLM_CALL_MULTI_PERFORM);
      if ($execrun != CURLM_OK) {
        break;
      }
      // Block a little to keep CPU time down
      curl_multi_select($mh, 0.001);
      while($done = curl_multi_info_read($mh)) {
        $url = curl_getinfo($done['handle'], CURLINFO_EFFECTIVE_URL);
        $this->esi_content[$url] = curl_multi_getcontent($done['handle']);
        // error_log("Done: " . $url);
        curl_multi_remove_handle($mh, $done['handle']);
        $content = str_replace($this->esi_urls[$url], $this->esi_content[$url], $content);
      }
    } while($running);
    // error_log('done multi curl');

    curl_multi_close($mh);

    return $content;
  }

  public function setCacheHeaders($content) {
    $this->time = time();
    $this->expires = $this->time + self::$timeout;

    if ($this->isCacheable()) {
      header("Cache-Control: private");
    }
    return $content;
  }

  public function recache($url) {
    drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
    // terror_log('recache');
    if (self::$queue && module_exists('drupal_queue')) {
      // terror_log('queuing');
      drupal_queue_include();
      $queue = DrupalQueue::get('http_accelerator');
      $queue->createItem($url);
      return TRUE;
    }
    elseif (function_exists('background_process_http_request')) {
      // @todo Background process here ...
      // drupal_http_request($url, array('User-Agent' => 'HTTP-Accelerator'));
      // terror_log('background process ...');
      $fp = background_process_http_request($url, NULL, array('headers' => array('User-Agent: HTTP-Accelerator'), 'method' => $_SERVER['REQUEST_METHOD']));
      return TRUE;
    }
    return FALSE;
  }

  public function http_request($url, $headers = array(), $method = 'GET', $data = NULL, $retry = 3) {
    global $db_prefix;

    $result = new stdClass();

    // Parse the URL and make sure we can handle the schema.
    $uri = parse_url($url);

    if ($uri == FALSE) {
      $result->error = 'unable to parse URL';
      $result->code = -1001;
      return $result;
    }

    if (!isset($uri['scheme'])) {
      $result->error = 'missing schema';
      $result->code = -1002;
      return $result;
    }

    switch ($uri['scheme']) {
      case 'http':
      case 'feed':
        $port = isset($uri['port']) ? $uri['port'] : 80;
        $host = $uri['host'] . ($port != 80 ? ':'. $port : '');
        $fp = @fsockopen($uri['host'], $port, $errno, $errstr, 15);
        break;
      case 'https':
        // Note: Only works for PHP 4.3 compiled with OpenSSL.
        $port = isset($uri['port']) ? $uri['port'] : 443;
        $host = $uri['host'] . ($port != 443 ? ':'. $port : '');
        $fp = @fsockopen('ssl://'. $uri['host'], $port, $errno, $errstr, 20);
        break;
      default:
        $result->error = 'invalid schema '. $uri['scheme'];
        $result->code = -1003;
        return $result;
    }

    // Make sure the socket opened properly.
    if (!$fp) {
      // When a network error occurs, we use a negative number so it does not
      // clash with the HTTP status codes.
      $result->code = -$errno;
      $result->error = trim($errstr);

      // Mark that this request failed. This will trigger a check of the web
      // server's ability to make outgoing HTTP requests the next time that
      // requirements checking is performed.
      // @see system_requirements()
      // variable_set('drupal_http_request_fails', TRUE);

      return $result;
    }

    // Construct the path to act on.
    $path = isset($uri['path']) ? $uri['path'] : '/';
    if (isset($uri['query'])) {
      $path .= '?'. $uri['query'];
    }

    // Create HTTP request.
    $defaults = array(
      // RFC 2616: "non-standard ports MUST, default ports MAY be included".
      // We don't add the port to prevent from breaking rewrite rules checking the
      // host that do not take into account the port number.
      'Host' => "Host: $host",
      'User-Agent' => 'User-Agent: Drupal (+http://drupal.org/)',
    );

    // Only add Content-Length if we actually have any content or if it is a POST
    // or PUT request. Some non-standard servers get confused by Content-Length in
    // at least HEAD/GET requests, and Squid always requires Content-Length in
    // POST/PUT requests.
    $content_length = strlen($data);
    if ($content_length > 0 || $method == 'POST' || $method == 'PUT') {
      $defaults['Content-Length'] = 'Content-Length: '. $content_length;
    }

    // If the server url has a user then attempt to use basic authentication
    if (isset($uri['user'])) {
      $defaults['Authorization'] = 'Authorization: Basic '. base64_encode($uri['user'] . (!empty($uri['pass']) ? ":". $uri['pass'] : ''));
    }

    // If the database prefix is being used by SimpleTest to run the tests in a copied
    // database then set the user-agent header to the database prefix so that any
    // calls to other Drupal pages will run the SimpleTest prefixed database. The
    // user-agent is used to ensure that multiple testing sessions running at the
    // same time won't interfere with each other as they would if the database
    // prefix were stored statically in a file or database variable.
    if (is_string($db_prefix) && preg_match("/^simpletest\d+$/", $db_prefix, $matches)) {
      $defaults['User-Agent'] = 'User-Agent: ' . $matches[0];
    }

    foreach ($headers as $header => $value) {
      $defaults[$header] = $header .': '. $value;
    }

    $request = $method .' '. $path ." HTTP/1.0\r\n";
    $request .= implode("\r\n", $defaults);
    $request .= "\r\n\r\n";
    $request .= $data;

    $result->request = $request;

    fwrite($fp, $request);

    // Fetch response.
    $response = '';
    while (!feof($fp) && $chunk = fread($fp, 1024)) {
      $response .= $chunk;
    }
    fclose($fp);

    // Parse response.
    list($split, $result->data) = explode("\r\n\r\n", $response, 2);
    $split = preg_split("/\r\n|\n|\r/", $split);

    list($protocol, $code, $status_message) = explode(' ', trim(array_shift($split)), 3);
    $result->protocol = $protocol;
    $result->status_message = $status_message;

    $result->headers = array();

    // Parse headers.
    while ($line = trim(array_shift($split))) {
      list($header, $value) = explode(':', $line, 2);
      if (isset($result->headers[$header]) && $header == 'Set-Cookie') {
        // RFC 2109: the Set-Cookie response header comprises the token Set-
        // Cookie:, followed by a comma-separated list of one or more cookies.
        $result->headers[$header] .= ','. trim($value);
      }
      else {
        $result->headers[$header] = trim($value);
      }
    }

    $responses = array(
      100 => 'Continue', 101 => 'Switching Protocols',
      200 => 'OK', 201 => 'Created', 202 => 'Accepted', 203 => 'Non-Authoritative Information', 204 => 'No Content', 205 => 'Reset Content', 206 => 'Partial Content',
      300 => 'Multiple Choices', 301 => 'Moved Permanently', 302 => 'Found', 303 => 'See Other', 304 => 'Not Modified', 305 => 'Use Proxy', 307 => 'Temporary Redirect',
      400 => 'Bad Request', 401 => 'Unauthorized', 402 => 'Payment Required', 403 => 'Forbidden', 404 => 'Not Found', 405 => 'Method Not Allowed', 406 => 'Not Acceptable', 407 => 'Proxy Authentication Required', 408 => 'Request Time-out', 409 => 'Conflict', 410 => 'Gone', 411 => 'Length Required', 412 => 'Precondition Failed', 413 => 'Request Entity Too Large', 414 => 'Request-URI Too Large', 415 => 'Unsupported Media Type', 416 => 'Requested range not satisfiable', 417 => 'Expectation Failed',
      500 => 'Internal Server Error', 501 => 'Not Implemented', 502 => 'Bad Gateway', 503 => 'Service Unavailable', 504 => 'Gateway Time-out', 505 => 'HTTP Version not supported'
    );
    // RFC 2616 states that all unknown HTTP codes must be treated the same as the
    // base code in their class.
    if (!isset($responses[$code])) {
      $code = floor($code / 100) * 100;
    }

    switch ($code) {
      case 200: // OK
      case 304: // Not modified
        break;
      case 301: // Moved permanently
      case 302: // Moved temporarily
      case 307: // Moved temporarily
        $location = $result->headers['Location'];

        if ($retry) {
          $result = $this->http_request($result->headers['Location'], $headers, $method, $data, --$retry);
          $result->redirect_code = $result->code;
        }
        $result->redirect_url = $location;

        break;
      default:
        $result->error = $status_message;
    }

    $result->code = $code;
    return $result;
  }

  function exitIfModified() {
    if (!$this->isModified()) {
      header("Not Modified", TRUE, 304);
      exit;
    }
  }

  function isModified() {
    $if_modified_since = isset($_SERVER["HTTP_IF_MODIFIED_SINCE"]) ? trim($_SERVER["HTTP_IF_MODIFIED_SINCE"]) : 0;
    if ($if_modified_since && $this->modified <= strtotime($if_modified_since)) {
      return FALSE;
    }
    return TRUE;
  }
 
  static public function initialize() {
    if (self::$initialized) {
      return FALSE;
    }

    self::$initialized = TRUE;
    include_once './includes/file.inc';
    global $base_root;
    self::$base_root = $base_root;
    self::$request_uri = request_uri();

    self::$cache_dir = getcwd() . '/' . variable_get('http_accelerator_cache_dir', file_directory_path() . '/http-accelerator');
    self::$cache = variable_get('http_accelerator_cache_enabled', FALSE);
    self::$esi = variable_get('http_accelerator_esi_enabled', FALSE);
    self::$timeout = variable_get('http_accelerator_timeout', HTTP_ACCELERATOR_TIMEOUT);
    self::$lock_timeout = variable_get('http_accelerator_lock_timeout', HTTP_ACCELERATOR_LOCK_TIMEOUT);
    self::$graceful_timeout = variable_get('http_accelerator_graceful_timeout', HTTP_ACCELERATOR_GRACEFUL_TIMEOUT);
    self::$queue = variable_get('http_accelerator_queue', FALSE);

    return TRUE;
  }

  static public function execute() {
    // terror_log(self::$request_uri . ': execute(' . self::$force . ')');
    $store = FALSE;
    $recache = FALSE;

    $cache = new HTTPAccelerator(self::$base_root . self::$request_uri);

    if (self::$cache && $cache->isCacheable() && $cache->exists() && $cache->load()) {
      if (!$cache->isExpired()) {
        $cache->exitIfModified();
      }
    }

    if (self::$esi) {
      ob_start(array($cache, 'applyESIContent'));
    }

    $force = isset($_SERVER['HTTP_USER_AGENT']) && $_SERVER['HTTP_USER_AGENT'] === 'HTTP-Accelerator' ? TRUE : FALSE;

    if (self::$cache && $cache->isCacheable()) {
      // terror_log(self::$request_uri . ': isCacheable');
      if ($force) {
        if ($cache->lock(TRUE)) {
          $cache->log('force');
          $store = TRUE;
        }
      }
      elseif ($cache->isExpired()) {
        // terror_log(self::$request_uri . ': expired');
        if ($cache->lock()) {
          if (self::$graceful_timeout > 0 && $cache->exists() && $_SERVER['REQUEST_TIME'] < $cache->expires + self::$graceful_timeout) {
            if ($cache->recache(self::$request_uri)) {
              $cache->log('Recaching gracefully');
            }
            else {
              $cache->log('Recaching now');
              $store = TRUE;
            }
          }
          else {
            $cache->log('Recaching now');
            $store = TRUE;
          }
        }
      }
      if (!$store && $cache->load()) {
        $cache->exitIfModified();
        // Serve cache if present
        $cache->deliverHeaders();
        print $cache->getContent();
        $cache->log('Serving cache');
        return TRUE;
      }
    }

    if ($store) {
      ob_start(array($cache, 'store'));
    }

    ob_start(array($cache, 'setCacheHeaders'));

    // terror_log(self::$request_uri . ': Generating page');
    return FALSE;
  }

  /**
   * Get request headers
   *
   * @return array headers
   */
  static public function requestHeaders() {
    static $headers = array();
    if (!empty($headers)) {
      return $headers;
    }

    foreach ($_SERVER as $key => $value) {
      if (substr($key, 0, 5) == 'HTTP_') {
        $key = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($key, 5)))));
        $headers[]= "$key: $value";
      }
    }
    return $headers;
  }

  function filterRequestHeaders() {
    $headers = self::requestHeaders();
    foreach ($headers as $header) {
      if (!preg_match('/^(Connection|Keep-Alive|Transfer-Encoding|Set-Cookie|Content-Length|Accept-Encoding|If-Modified-Since|If-None-Match):/i', $header)) {
        $result[] = $header;
      }
    }
    return $result;
  }

  function filterResponseHeaders() {
    $headers = headers_list();
    foreach ($headers as $header) {
      if (!preg_match('/^(Content-Encoding):/i', $header)) {
        $result[] = $header;
      }
    }
    return $result;
  }
}

