<?php

/**
 * FAPI definition for the HTTP Accelerator settings
 *
 * @ingroup forms
 */
function http_accelerator_settings_form() {
  $form = array();

  $form['http_accelerator_enabled'] = array(
    '#title'         => t('Enable HTTP Accelerator'),
    '#type'          => 'checkbox',
    '#default_value' => variable_get('http_accelerator_enabled', FALSE),
  );
  $form['http_accelerator_cache_enabled'] = array(
    '#title'         => t('Enable caching'),
    '#type'          => 'checkbox',
    '#default_value' => variable_get('http_accelerator_cache_enabled', FALSE),
  );
  $form['http_accelerator_esi_enabled'] = array(
    '#title'         => t('Enable ESI processing'),
    '#type'          => 'checkbox',
    '#default_value' => variable_get('http_accelerator_esi_enabled', FALSE),
  );
  $form['http_accelerator_loglevel'] = array(
    '#title'         => t('Loglevel'),
    '#type'          => 'select',
    '#default_value' => variable_get('http_accelerator_loglevel', WATCHDOG_ERROR),
    '#options'       => array(
      WATCHDOG_EMERG      => t('Emergency'),
      WATCHDOG_ALERT      => t('Alert'),
      WATCHDOG_CRITICAL   => t('Critical'),
      WATCHDOG_ERROR      => t('Error'),
      WATCHDOG_WARNING    => t('Warning'),
      WATCHDOG_NOTICE     => t('Notice'),
      WATCHDOG_INFO       => t('Info'),
      WATCHDOG_DEBUG      => t('Debug'),
    ),
  );
  $form['http_accelerator_timeout'] = array(
    '#title'         => t('Timeout'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('http_accelerator_timeout', HTTP_ACCELERATOR_TIMEOUT),
    '#description'   => t('Time out for the cache objects'),
  );
  $form['http_accelerator_graceful_timeout'] = array(
    '#title'         => t('Graceful timeout'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('http_accelerator_graceful_timeout', HTTP_ACCELERATOR_GRACEFUL_TIMEOUT),
    '#description'   => t('Timeout for graceful period'),
  );
  $form['http_accelerator_lock_timeout'] = array(
    '#title'         => t('Lock timeout'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('http_accelerator_lock_timeout', HTTP_ACCELERATOR_LOCK_TIMEOUT),
    '#description'   => t('Time out for lock files'),
  );
  $form['http_accelerator_cache_dir'] = array(
    '#title'         => t('Cache directory'),
    '#type'          => 'textfield',
    '#default_value' => variable_get('http_accelerator_cache_dir', file_directory_path() . '/http-accelerator/cache'),
    '#description'   => t('The directory where the HTTP Accelerator cache files are stored'),
  );
  if (module_exists('drupal_queue')) {
    $form['http_accelerator_queue'] = array(
      '#title'         => t('Queue'),
      '#type'          => 'checkbox',
      '#default_value' => variable_get('http_accelerator_queue', FALSE),
      '#description'   => t('Use queue processing for graceful recache'),
    );
  }


  $form['configuration'] = array(
    '#title'         => t('Tips'),
    '#type'          => 'fieldset',
    '#collapsed'     => TRUE,
    '#collapsible'   => TRUE,
  );

  $tips = _http_accelerator_get_tips();
  $form['configuration']['htaccess'] = array(
    '#title' => 'htaccess',
    '#type' => 'textarea',
    '#disabled' => TRUE,
    '#value' => $tips['htaccess'],
  );
  $form['configuration']['fastpath'] = array(
    '#title' => 'fastpath',
    '#type' => 'textarea',
    '#disabled' => TRUE,
    '#value' => $tips['fastpath'],
  );

  return system_settings_form($form);
}

function _http_accelerator_get_tips() {
  $cache_dir = variable_get('http_accelerator_cache_dir', file_directory_path() . '/http-accelerator/cache');
  $full_cache_dir = realpath($cache_dir);
  $full_cache_dir = str_replace(realpath($_SERVER['DOCUMENT_ROOT']), '%{DOCUMENT_ROOT}', $cache_dir);

  $message['htaccess'] = "Please use the following in .htaccess for full static file support:

  # Skip cache if not applicable
  RewriteCond %{REQUEST_METHOD} !^(GET|HEAD)$ [OR]
  RewriteCond %{HTTP:Pragma} no-cache [OR]
  RewriteCond %{HTTP:Cache-Control} no-cache [OR]
  RewriteCond %{HTTPS} on [OR]
  RewriteCond %{HTTP_USER_AGENT} HTTP-Accelerator
  RewriteRule .* - [S=7]

  # Setup HTTP Accelerator variables
  SetEnvIf Server_Addr ^.*$ HTTP_ACCELERATOR_CACHE_DIR=$cache_dir
  SetEnvIf Server_Addr ^.*$ HTTP_ACCELERATOR_CONTENT_GROUP=default

  # Create request string, converting slashes to {}
  RewriteCond %{ENV:ENC_REQUEST_URI} !^(.+)$
  RewriteCond %{REQUEST_URI} ^(.*)$
  RewriteRule .* - [E=ENC_REQUEST_URI:%1]
  RewriteCond %{ENV:ENC_REQUEST_URI} !^/$cache_dir [NC]
  RewriteCond %{ENV:ENC_REQUEST_URI} ^(.*)/(.*)$ [NC]
  RewriteRule .* - [E=ENC_REQUEST_URI:%1\{\}%2,N]

  # Create query string with double encoded slashes
  RewriteCond %{ENV:ENC_QUERY_STRING} !^(.+)$
  RewriteCond %{QUERY_STRING} ^(.*)$
  RewriteRule .* - [E=ENC_QUERY_STRING:%1]
  RewriteCond %{ENV:ENC_QUERY_STRING} ^(.*)%2f(.*)$ [NC]
  RewriteRule .* - [E=ENC_QUERY_STRING:%1\%252f%2,N]

  # Switch content group if logged in
  RewriteCond %{HTTP_COOKIE} SESS
  RewriteRule .* - [E=HTTP_ACCELERATOR_CONTENT_GROUP:users]

  # Choose sub-directory based on query-string
  RewriteCond %{QUERY_STRING} !^(.+)$
  RewriteRule .* - [E=HTTP_ACCELERATOR_QUERY_STRING:noqs]
  RewriteCond %{QUERY_STRING} ^(.+)$
  RewriteRule .* - [E=HTTP_ACCELERATOR_QUERY_STRING:qs.]

  RewriteCond %{DOCUMENT_ROOT}/%{ENV:HTTP_ACCELERATOR_CACHE_DIR}/%{ENV:HTTP_ACCELERATOR_CONTENT_GROUP}/%{SERVER_NAME}/%{ENV:HTTP_ACCELERATOR_QUERY_STRING}%{QUERY_STRING}/%{ENV:ENC_REQUEST_URI}/index\.php -s
  RewriteRule .* %{ENV:HTTP_ACCELERATOR_CACHE_DIR}/%{ENV:HTTP_ACCELERATOR_CONTENT_GROUP}/%{SERVER_NAME}/%{ENV:HTTP_ACCELERATOR_QUERY_STRING}%{ENV:ENC_QUERY_STRING}/%{ENV:ENC_REQUEST_URI}/index\.php [S=1]

";

  $message['fastpath'] .= '
  Please use the following in your settings file, for faster delivery of cache hits (bypass db):

  $conf[\'http_accelerator_enabled\'] = ' . var_export(variable_get('http_accelerator_enabled', FALSE), TRUE) . ';
  $conf[\'http_accelerator_cache_enabled\'] = ' . var_export(variable_get('http_accelerator_cache_enabled', FALSE), TRUE) . ';
  $conf[\'http_accelerator_esi_enabled\'] = ' . var_export(variable_get('http_accelerator_esi_enabled', FALSE), TRUE) . ';
  $conf[\'http_accelerator_timeout\'] = ' . var_export(variable_get('http_accelerator_timeout', HTTP_ACCELERATOR_TIMEOUT), TRUE) . ';
  $conf[\'http_accelerator_graceful_timeout\'] = ' . var_export(variable_get('http_accelerator_graceful_timeout', HTTP_ACCELERATOR_GRACEFUL_TIMEOUT), TRUE) . ';
  $conf[\'http_accelerator_loglevel\'] = ' . var_export(variable_get('http_accelerator_loglevel', WATCHDOG_ERROR), TRUE) . ';
  $conf[\'http_accelerator_cache_dir\'] = ' . var_export(variable_get('http_accelerator_cache_dir', file_directory_path() . '/http-accelerator/cache'), TRUE) . ';
  $conf[\'http_accelerator_lock_timeout\'] = ' . var_export(variable_get('http_accelerator_lock_timeout', HTTP_ACCELERATOR_LOCK_TIMEOUT), TRUE) . ';
  $conf[\'http_accelerator_queue\'] = ' . var_export(variable_get('http_accelerator_queue', FALSE), TRUE) . ';
  include \'' . drupal_get_path('module', 'http_accelerator') . '/http_accelerator.inc\';
';

  return $message;
}

